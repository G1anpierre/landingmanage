import React, { Component } from 'react';
import MyNavbar from './components/Navbar';
import HomePage from './pages/HomePage';
import { BrowserRouter, Switch, Route } from 'react-router-dom';


class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      page: 1,
      pending: false,
      info: {},
      data: {
        results: [],
      },
      error: null,
    }
  }

  componentDidMount() {
    this.fetchCharacters();
  }

  fetchCharacters = () => {
    this.setState({
      page: this.state.page + 1,
      pending: true,
      error: null 
    })
    fetch(`https://rickandmortyapi.com/api/character?page=${this.state.page}`)
    .then(response => response.json())
    .then(data => {
      this.setState({
        pending: false,
        info: data.info,
        data: {
          results: [...this.state.data.results, ...data.results],
        },
        error: null
      })
    }). catch(
      this.setState({
        error: 'hubo error'
      })
    )
  }

  render() {

    const Home = () => {
      return <HomePage data={this.state} fetchCharacters={this.fetchCharacters} />
    }

    return (
      <BrowserRouter>
        <MyNavbar />
        <Switch>
          <Route path="/" component={Home} />
        </Switch>
      </BrowserRouter>
      
    )
  }
}

export default App;
