import React, { Component } from 'react'
import FirstComponent from '../components/FirstComponent';
import SecondComponent from '../components/SecondComponent';
import VideoComponent from '../components/VideoComponent';
import TestimonialsCarousel from '../components/TestimonialsCarousel';
import CarrouselComponent from '../components/Carrousel';
import RickAndMorty from '../components/RickAndMorty';

export default class HomeComponent extends Component {

    constructor(props) {
        super(props);

    }

    render() {
        return (
            <main>
                <FirstComponent />
                <SecondComponent />
                <VideoComponent />
                <CarrouselComponent />
                <TestimonialsCarousel />
                <RickAndMorty state={this.props.data} fetch={this.props.fetchCharacters}/>
            </main>
        )
    }
}
