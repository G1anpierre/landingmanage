import React, { Component } from 'react'
import { Container, Navbar, NavbarBrand, Nav, NavItem, Button, NavbarToggler, Collapse} from 'reactstrap';
import { NavLink } from 'react-router-dom';
import Logo from '../images/logo.svg';


export default class MyNavbar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isOpen: false
        }
    }

    toggle = () => {
        this.setState({
            isOpen: !this.state.isOpen
        })
    }



    render() {
        return (
            <Container>
                <Navbar color="light" expand="lg" light className="navbar-component">
                    <NavbarBrand href="/">
                        <img src={Logo}/>
                    </NavbarBrand>
                    <NavbarToggler onClick={this.toggle} />

                    <Collapse isOpen={this.state.isOpen} navbar >
                        <Nav className="mx-auto" navbar >
                            <NavItem>
                                <NavLink className="nav-link" to="#">Pricing</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="nav-link" to="#">Product</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="nav-link" to="#">About Us</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="nav-link" to="#">Careers</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="nav-link" to="#">Community</NavLink>
                            </NavItem>
                        </Nav>
                        <Nav className="button-container">
                            <NavItem>
                                <Button >Get Started</Button>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </Navbar>
            </Container>
        )
    }
}
