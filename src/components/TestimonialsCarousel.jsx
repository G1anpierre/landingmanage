import React, { Component } from 'react';
import { Card, CardTitle, CardText, Row, Col, Button, Media } from 'reactstrap';
import AvatarAnisha from '../images/avatar-anisha.png';
import AvatarRichrad from '../images/avatar-richard.png';
import AvatarShanai from '../images/avatar-shanai.png';


export default class TestimonialsCarousel extends Component {

    constructor(props) {
        super(props);      
        
    }
    
    render() {
     
        return (
            <section className="testimonial-component">
               
                <div className="text-center my-5">
                    <h1>What They've said</h1>
                </div>
                <div className="testimonial-component__carousel">
                    <Row>
                        <Col>
                            <Card body className="text-center">
                                <Media object className="testimonial-image" src={AvatarAnisha} alt="Generic placeholder image" />
                                <CardTitle>Special Title Treatment</CardTitle>
                                <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                                <Button>Go somewhere</Button>
                            </Card>
                        </Col>
                        <Col>
                            <Card body className="text-center">
                                <Media object className="testimonial-image" src={AvatarRichrad} alt="Generic placeholder image" />
                                <CardTitle>Special Title Treatment</CardTitle>
                                <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                                <Button>Go somewhere</Button>
                            </Card>
                        </Col>
                        <Col>
                            <Card body className="text-center">
                                <Media object className="testimonial-image" src={AvatarShanai} alt="Generic placeholder image" />
                                <CardTitle>Special Title Treatment</CardTitle>
                                <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                                <Button>Go somewhere</Button>
                            </Card>
                        </Col>
                    </Row>
                </div>

            </section>
        )
    }
}
