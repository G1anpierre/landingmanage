import React, { Component } from 'react'
import { Container, Row, Col, Badge} from 'reactstrap';

export default class SecondComponent extends Component {
    render() {
        return (
            <Container>
                <section className="second-component">
                        <Row>
                            <Col xs="12" md="6">
                                <div className="second-component__text-content">
                                        <h3 className="second-component__title">What's different about Manage?</h3>
                                        <p>Manage provides all the functionality your team needs, without the complexity. Our software is tailor-made for modern digital product teams</p>
                                </div>
                            </Col>
                            <Col xs="12" md="6">
                                <div className="second-component__features">
                                    <div className="feature">
                                        <Row>
                                            <Col xs="2">
                                                <h4 className="text-center"><Badge>01</Badge></h4>
                                            </Col>
                                            <Col xs="10" className="d-flex align-items-center">
                                                <h6>Track company-wide progress</h6>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col xs={{size: 12}}  sm={{size:10, offset: 2}}>
                                                See how your day-to-day tasks fit into the wider vision. Go from  tracking progress at the milestone level all the way done to the smallest of details. 
                                                Never lose sight of the bigger picture again.
                                            </Col>
                                        </Row>
                                    </div>
                                    <div className="feature">
                                        <Row>
                                            <Col xs="2">
                                                <h4 className="text-center"><Badge>02</Badge></h4>
                                            </Col>
                                            <Col xs="10" className="d-flex align-items-center">
                                                <h6>Track company-wide progress</h6>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col xs={{size: 12}}  sm={{size:10, offset: 2}}>
                                                See how your day-to-day tasks fit into the wider vision. Go from  tracking progress at the milestone level all the way done to the smallest of details. 
                                                Never lose sight of the bigger picture again.
                                            </Col>
                                        </Row>
                                    </div>
                                    <div className="feature">
                                        <Row>
                                            <Col xs="2">
                                                <h4 className="text-center"><Badge>03</Badge></h4>
                                            </Col>
                                            <Col xs="10" className="d-flex align-items-center">
                                                <h6>Track company-wide progress</h6>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col xs={{size: 12}}  sm={{size:10, offset: 2}}>
                                                See how your day-to-day tasks fit into the wider vision. Go from  tracking progress at the milestone level all the way done to the smallest of details. 
                                                Never lose sight of the bigger picture again.
                                            </Col>
                                        </Row>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </section>
                </Container>
        )
    }
}
