import React, { Component } from 'react';
import { Jumbotron, Card, CardTitle, CardText, CardImg, CardImgOverlay, Button, Container, Row, Col } from 'reactstrap';
import VisibilitySensor from 'react-visibility-sensor';
import { Loading } from './Loader';


class RickAnMorty extends Component {

    constructor(props) {
        super(props);

    }


    renderCard = (results) => {
    
        return (
            <>
              <Col>
                { results.map( element => (
                      <Card inverse className="mb-3" id={element.id}>
                        <CardImg width="100%" src={element.image} alt={element.name} />
                        <CardImgOverlay>
                          <CardTitle>{element.name}</CardTitle>
                          <CardText>
                            <small className="text-muted">{element.species}</small>
                          </CardText>
                        </CardImgOverlay>
                      </Card>
                  ))
                }
              </Col>
            </>
         )
    }

    render() {

      const { state: { pending, data: { results } }, fetch } = this.props;

      const RenderCard2 = ({values}) => {
        

        return (
            <>
                { values.map( element => {
                    return  (
                            <VisibilitySensor key={element.id} >
                                {({ isVisible }) => {
                                    return (
                                        <Col>
                                            <Card inverse className={`mb-3 ${isVisible ? 'full' : 'medium'}`}  >
                                                <CardImg width="100%" src={element.image} alt={element.name} />
                                                <CardImgOverlay>
                                                <CardTitle>{element.name}</CardTitle>
                                                <CardText>
                                                    <small className="text-muted">{element.species}</small>
                                                </CardText>
                                                </CardImgOverlay>
                                            </Card>
                                         </Col>
                                    )

                                }}
                                        
                            </VisibilitySensor>
                    ) 
                })}
            </>
            )

            
        }

        return(
            <div className="rick-and-morty">
                <Jumbotron>
                    <h1 className="App__title">Rick and Morty</h1>
                </Jumbotron>
                <Container>
                    <Row xs="1" sm="2" md="4">
                        {pending 
                        ? (
                            <Loading /> 
                            )
                        : (
                            <VisibilitySensor 
            
                            onChange={(isVisible) => {this.setState({cardVisible: isVisible})}}
                            partialVisibility
                            >
                            <RenderCard2 values={results} />
                            </VisibilitySensor>
                            // this.renderCard(results)
                        )
                        }
                    </Row>
                    <Row>
                        <Col>
                            <Button color="success" className="my-3" onClick={fetch}>Charge More</Button>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }

}


export default RickAnMorty;