import React, { Component } from 'react';

import { CarouselItem, Carousel, CarouselIndicators, CarouselControl, CarouselCaption } from 'reactstrap';


const items = [
    {
      src: 'https://images.unsplash.com/photo-1587493710442-14302d0f8d82?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80',
      title: 'Tesla Red',
      testimonial: 'Slide S'
    },
    {
      src: 'https://images.unsplash.com/photo-1576221162298-3d9f04e9f661?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80',
      title: 'Tesla Black',
      testimonial: 'Model 3'
    },
    {
      src: 'https://images.unsplash.com/photo-1600661653561-629509216228?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1498&q=80',
      title: 'Tesla White',
      testimonial: 'Model Y'
    }
  ];

  class CarrouselComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            activeIndex: 0,
            animating: false
        }
    }

    setAnimating = () => {
        this.setState({
            animating: !this.state.animating
        })
    }

    next = () => {
        if (this.state.animating) return;
        const newIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex + 1;
        this.setState({
            activeIndex: newIndex
        })
    }

    previous = () => {
        if (this.state.animating) return;
        const newIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex - 1;
        this.setState({
            activeIndex: newIndex
        })
    }

    goToIndex = (newIndex) => {
        if (this.state.animating) return;
        this.setState({
            activeIndex: newIndex
        })
    }


    render() {

        const slides = items.map((item) => {
            return (
              <CarouselItem
                onExiting={this.setAnimating}
                onExited={this.setAnimating}
                key={item.src}
              >

                    <img src={item.src} alt="Generic placeholder image" />
                    <CarouselCaption captionText={item.title} captionHeader={item.testimonial}/>

              </CarouselItem>
            );
        });


        return (
            <Carousel
                    activeIndex={this.state.activeIndex}
                    next={this.next}
                    previous={this.previous}
                >
                <CarouselIndicators 
                    items={items} activeIndex={this.state.activeIndex} onClickHandler={this.goToIndex}
                    /> 

                {slides}

                <CarouselControl 
                        direction="prev" directionText="Previous" onClickHandler={this.previous}
                    /> 
                <CarouselControl 
                        direction="next" directionText="Next" onClickHandler={this.next}
                    /> 
            </Carousel>
        )
    }

  }

export default CarrouselComponent;