import React, { Component } from 'react'
import { Container, Row, Col, Button} from 'reactstrap';
import Ilustration from '../images/illustration-intro.svg'


export default class FirstComponent extends Component {
    render() {
        return (
            <Container>
                <section className="first-component">
                    <Row className="align-items-center justify-content-between">
                        <Col xs={{ size: 12, order: 2}} md={{ size: 6, order: 1}}>
                            <div className="first-component__text-content">
                                <h1 className="first-component__title">Bring everyone together to build better products</h1>
                                <p className="first-component__paragraph">Manage makes it simple for software teams to plan day-to-day tasks while keeping the larger team in view.</p>
                                <Button>Get Started</Button>
                            </div>

                        </Col>
                        <Col xs={{ size: 12, order: 1}} md={{ size: 6, order: 2}}>
                            <div className="first-component__image-alignment">
                                <div className="ilustration-image-container">
                                    <img className='ilustration-image' src={Ilustration}></img>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </section>
            </Container>
        )
    }
}
